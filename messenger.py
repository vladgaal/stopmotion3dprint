import os


def show(index, **kwargs):

    if os.name == 'nt':
        os.system('cls')
    elif os.name == 'posix':
        os.system('clear')

    cropping = kwargs.get('crop')
    rotating = kwargs.get('rotate')
    path = kwargs.get('path')
    prefix = kwargs.get('prefix')
    output = kwargs.get('output')
    out_type = ''
    crop_par = ['x = ', 'y = ', 'width = ', 'height = ']

    if index == 1:
        print("Converting set of pictures to a GIF")
        out_type = '.gif'
    elif index == 2:
        print("Converting set of pictures to a video")
        out_type = '.avi'

    if path:
        print("In folder: ", path)
    if prefix:
        print("The files starts with: ", prefix)
    if rotating:
        print("Rotating: ", str(rotating) + "°")
    if cropping:
        print("Cropping: ", *[crop_par[i] + str(cropping[i]) + 'px;' for i in range(len(cropping))])
    if output:
        print("The output file is: ", output + out_type)
