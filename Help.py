import os


def show(index=0):
    # Read the help.txt file and print the corresponding message
    with open("help.txt", "r", newline='\n') as file:
        for line in file.readlines():
            if os.name == 'nt':
                line = line.replace('\r', '')
            if len(line.rstrip('\n')) > 0:
                if int(line.rstrip('\n')[0]) == index or int(line.rstrip('\n')[0]) == 0:
                    print(line.rstrip('\n')[1:])
            else:
                print('')


if __name__ == '__main__':
    show(index=1)
