# StopMotion3DPrint #

Set of circuits, codes and parts to generate awesome Stop Motion videos of your 3D prints.
When you try to make a Stop Motion of a 3D print the nozzle is in a different position on each shot, heaving a not so great result. 
The solution is to sync the camera with the printer. 
In this project I handle three diferrents approaches tho have a nice Stop Motion of a print.

------

## What I am using in my tests ##

* 3D Printer: Homemade FDM printer, controlled by arduino mega2560 and Ramps 1.4 and firmware Repetier ;
* Repetier Host;
* Slic3r;
* Raspberry Pi 3 B+;
* SJ4000 SJCAM (GoPro like);
* Nikon D7000;
* Manual Remote Trigger Mc-dc2 (disassembled to use just the wire and connector);
* Arduino 5V relay module;
* Raspberry Pi camera module Rev 1.3;

------

## Approaches ##

I will cover three different approaches to make the Stop Motion:

1. A USB-Camera connected with a raspberry pi powered and triggered by the Ramps + Arduino;
1. A Raspberry Camera (flat cable connected) with a raspberry pi powered and triggered by the Ramps + Arduino;
1. A DSLR camera triggered (Relay) by the Ramps + Arduino;

##### Schematic of the approach 1 and 2 #####

![Alt text](./Pics/Schematic.png "Project schematic"){height=50px}

##### Schematic of the approach 3 #####

![Alt text](./Pics/schem3.png "Project schematic"){height=50px}

------

## Approach 1 and 2 ##

### Step by step to use ###

![Alt text](./Pics/Flowchart.png "Project Flowchart"){height=50px}

### 1. Configuring the Raspberry Pi ###

In both cases that a Raspberry Pi is used the initial configuration is the same:

* Install a Raspberry distro: I've used [this](https://stadt-bremerhaven.de/raspberry-pi-os-installieren-ssh-vnc-und-wlan-konfigurieren/) tutorial, basically choose a distro, enable SSH, enable VNC and configure a WLAN;
* Update the OS, install PIP3 and OpenCV:
```
sudo apt -y update
sudo apt -y upgrade
sudo apt-get install python3-pip
pip3 install opencv-python
pip3 install imageio
pip3 install numpy
```
* Clone this repository;
* To install and configure the camera module follow [this](https://projects.raspberrypi.org/en/projects/getting-started-with-picamera) tutorial
* [Here](https://picamera.readthedocs.io/en/release-1.10/api_camera.html#picamera) is the PiCamera documentation

### 2. Clone the repository ###

Use the git clone command to clone from: ```git clone https://bitbucket.org/vladgaal/stopmotion3dprint.git```

### 3. Configure a shared folder ###

Configure a shared folder to send the shots to others computers (optional). I've used [this](https://raspberrypihq.com/how-to-share-a-folder-with-a-windows-computer-from-a-raspberry-pi/) tutorial;

### 4. Electronics ###

The electronics for the approach 1 and 2 are the same and consist of 3 Leds (Red to power, Green to operation and Yellow to "taking a shot") drivered by transistors, a transistor working as a button  commanded by the printer electronics (Ramps + Arduino) and a DC-DC step down circuit.
The objective of this electrics is to power the Raspberry Pi, indicate the state of the programm thought the Leds and receive the trigger from the printer.

The schematics and some pictures of the first prototype:

![Alt text](./Pics/circuit_schem.png "Circuit Schematic "){height=100}
![Alt text](./Pics/Prototype_Circuit_a.jpg "Circuit front"){height=100}
![Alt text](./Pics/Prototype_Circuit_b.jpg "Circuit back"){height=100}
![Alt text](./Pics/Prototype_Circuit_Raspberry.jpg "Circuit + Raspberry"){height=100}

### 5. GCODE ###

The G-CODE will give the command to trigger the camera. 
I believe that the best way is to write a set of commands on the "Printer Settings" at the box "After layer change".
Make sure to mark the option "Retract on layer change" in the Extruder tab of the printer settings.
Using these two settings the printer will first retract and then move and take the shoot.

The set of commands move the nozzle to a fixed possition, wait the motors to stop, and change the pins state to trigger the camera. 
The fixed nozzle position depends on the part dimensions and the user need think and set the best location. 

Type the following commands on the "Printer Settings" at the box "After layer change":
```
G1 X100 Y100  ; Move the nozzle in every layer to the same point
M400          ; Wait the motor stops
M42 P4 S255   ; Set pin D4 to high
G4 P100       ; Wait 100 ms
M42 P4 S0     ; Set pin D4 to low
G4 P1000      ; Wait 1 s
```

### 6. The Raspberry Pi scripts ###

###### Approach 1 ######

* Use the file ```camera_USB_preview.py``` to take a single shot using the USB camera and try the parameters.
* Use the file ```main_camera_USB.py``` to run the main routine and use the trigger to take the shots using the USB camera. The main file accepts arguments to the destination folder (-p) and photos format (-f):
```
python3 main_camera_USB.py -p </path/to/the/folder/> -f <.png>
```

###### Approach 2 ######

* Use the file ```camera_module_preview.py``` to take a single shot using the camera module and try the parameters.
* Use the file ```main_camera_module.py``` to run the main routine and use the trigger to take the shots using the camera module. The main file accepts arguments to the destination folder (-p) and photos format (-f):
```
python3 main_camera_module.py -p </path/to/the/folder/> -f <.png>
```

### 7. Post-processing scripts ###

Use the file ```postProcessing_fig2video.py``` to convert the set of pictures to a video or the ```postProcessing_fig2gif.py``` to convert to a gif.
They both have the same arguments:

```
-h                         Show help.
-i, --inpath    <path>     Path to the pictures folder
-n, --name      <name>     Prefix of the pictures
-o, --outfile   <name>     Name of the output file
-f, --format    <format>   Pictures extension
-c, --crop                 To crop the pictures when converting
-x, --vecX      <pixels>   Start point X of the cropping
-y, --vecY      <pixels>   Start point y of the cropping
-w, --vecW      <pixels>   Width of the crop
-a, --vecH      <pixels>   Height of the crop
-r, --rotate    <angle>    Rotate angle
-q, --framerate <value>    Video frame rate (just for video)
```

----

## Approach 3 ##

### Step by step  use ###

![Alt text](./Pics/Flowchart3.png "Project Flowchart"){height=50px}

### 1. Clone the repository ###

**Windows users**

If you don't yet have:

* [download and install git](https://git-scm.com/downloads). Use the git clone command: ```git clone https://bitbucket.org/vladgaal/stopmotion3dprint.git```
* Download [here](https://www.python.org/downloads/windows/) and install the latest version of Python 3.7.
* Go to the cloned folder ".\Dependencies" and execute the "install_all.bat" to install everything that you need to run.

**Linux users**

Use the git clone command: ```git clone https://bitbucket.org/vladgaal/stopmotion3dprint.git```

Install/update the following libraries:

```
sudo apt-get install python3-pip
pip3 install opencv-python
pip3 install imageio
pip3 install numpy
```

### 2. Electronics ###

The electronics for the approach 3 consist of two transistor drivered relay triggered by the printer electronics (Ramps + Arduino).
The objective of this electrics is to give a trigger to the DSLR Camera. 
Two relays are used because the camera needs a pre-trigger to set the photometry and then a second to take the shot.
The external trigger cable is supposed to have three wires, a common, a pre-trigger, and a trigger. 
In my case the is common, the green is pre-trigger, and the red is trigger.
You need to try your cable to find the right combination, it is not so hard and not even dangerous, just try 2 by 2 combination.
There is a jumper nearby the servos pins, this jumper sets the power source of the servos.
By default, it is set to an external source, but in our case, it should be set to send Arduino's +5V.

![Alt text](./Pics/circuit3.png "Circuit Schematic "){height=100}

### 3. GCODE ###

The G-CODE will give the command to trigger the camera. 
I believe that the best way is to write a set of commands on the "Printer Settings" at the box "After layer change".
Make sure to mark the option "Retract on layer change" in the Extruder tab of the printer settings.
Using these two settings the printer will first retract and then move and take the shoot.

The set of commands move the nozzle to a fixed possition, wait the motors to stop, and change the pins state to trigger the camera. 
The fixed nozzle position depends on the part dimensions and the user need think and set the best location. 

Type the following commands on the "Printer Settings" at the box "After layer change":
```
G1 X90 Y90    ; Move the nozzle in every layer to the same point
M400          ; Wait the motor stops
M42 P4 S255   ; Set pin D4 to high
G4 P1000      ; Wait 1 s
M42 P5 S255   ; Set pin D5 to high
G4 P1000      ; Wait 1 s
M42 P4 S0     ; Set pin D4 to low
M42 P5 S0     ; Set pin D4 to low
```

### 4. Setting the camera ###

Using a DSLR you have a lot of variables to take a photo. It would imply in have differences between the photos of a print. 
Because of that use the manual mode to fix the aperture, exposition time, and ISO, use the photometer to check if the brightness is ok. 
Set the focus to manual and set a focus point. 
Using this configuration you shall have a pretty good uniformity during the print, and don't forget to keep the same ambient luminosity.

### 5. Post-processing scripts ###

Use the file ```postProcessing_fig2video.py``` to convert the set of pictures to a video or the ```postProcessing_fig2gif.py``` to convert to a gif.
They both have the same arguments:

```
-h                         Show help.
-i, --inpath    <path>     Path to the pictures folder
-n, --name      <name>     Prefix of the pictures
-o, --outfile   <name>     Name of the output file
-f, --format    <format>   Pictures extension
-c, --crop                 To crop the pictures when converting
-x, --vecX      <pixels>   Start point X of the cropping
-y, --vecY      <pixels>   Start point y of the cropping
-w, --vecW      <pixels>   Width of the crop
-a, --vecH      <pixels>   Height of the crop
-r, --rotate    <angle>    Rotate angle
-q, --framerate <value>    Video frame rate (just for video)
```

----