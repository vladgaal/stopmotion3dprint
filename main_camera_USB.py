import RPi.GPIO as GPIO
import sys, getopt
import time
import cv2
import Help

Led1Pin = 8    # pin8  --- Green led pin
Led2Pin = 10   # pin11 --- Yellow led pin
BtnPin = 12    # pin12 --- In signal pin

global numero
numero = 0     # shot number
global path
path = ''      # path to folder to save the Pic
global form
form = '.png'      # Pic extension

def skip(qtd=0):
    # Function to skip "qtd" shots
    
    for i in range(qtd):
        return_value, image = camera.read()
        time.sleep(0.1)

def setup():
    # Setup routine, run just at start
    
    global camera
    
    camera = cv2.VideoCapture(0)    # Start the camera
    
    if not camera.isOpened(): return False
    
    camera.set(3, 1280)
    camera.set(4, 720)

    GPIO.setmode(GPIO.BOARD)        # Numbers GPIOs by physical location
    GPIO.setup(Led1Pin, GPIO.OUT)   # Set Green LedPin's mode is output
    GPIO.setup(Led2Pin, GPIO.OUT)   # Set Yellow LedPin's mode is output
    GPIO.setup(BtnPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)    # Set BtnPin's mode is input, and pull up to high level(3.3V)
    GPIO.output(Led1Pin, GPIO.LOW)  # Set Green LedPin low to off led
    GPIO.output(Led2Pin, GPIO.LOW)  # Set Yellow LedPin low to off led
    
    skip(qtd=6) # Skip the first 6 shots, due to some problem with the webcam
    
    return True
    

def shot(ev=None):
    # Function executed whenever interruption occurs
    
    GPIO.output(Led2Pin, GPIO.HIGH) # Set Yellow LedPin high
    
    global numero
    global path
    global form
    
    return_value, image = camera.read() # Get camera image
    cv2.imwrite(path + 'Pic_' + str(numero).zfill(3) + form , image) # Save the image
    print('Shot: ' + str(numero).zfill(3))
    
    numero += 1
    time.sleep(0.1)
    
    GPIO.output(Led2Pin, GPIO.LOW) # Set Yellow LedPin low to off led
    

def loop():
    # Main loop routine
    
    GPIO.add_event_detect(BtnPin, GPIO.FALLING, callback=shot, bouncetime=200) # wait for falling and set bouncetime to prevent the callback function from being called multiple times when the button is pressed
    while True:
        #print('running')
        # Blink the green led
        time.sleep(1)
        GPIO.output(Led1Pin, GPIO.HIGH)
        time.sleep(1)
        GPIO.output(Led1Pin, GPIO.LOW)


def destroy():
    # Routine to close everything
    
    print('end')
    GPIO.output(Led1Pin, GPIO.LOW)     # led off
    GPIO.output(Led2Pin, GPIO.LOW)     # led off
    GPIO.cleanup()
    camera.release()


def main(argv):
    global path
    global form
    try:
        opts, args = getopt.getopt(argv,"hp:f:",["path=","form="])
    except getopt.GetoptError:
        print('Incorrect input')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            Help.show(index=1)
            sys.exit()
        elif opt in ("-p", "--path"):
            path = arg
        elif opt in ("-f", "--form"):
            form = arg
            
    print(path, form)
    
    if not setup():
        print("Problem loading camera")
        return False
    try:
        loop()
    except KeyboardInterrupt:  # When 'Ctrl+C' is pressed, the child program destroy() will be  executed.
        destroy()
    
    
if __name__ == '__main__':
    main(sys.argv[1:])
    
