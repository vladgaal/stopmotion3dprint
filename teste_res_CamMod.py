from picamera import PiCamera
import time

camera = PiCamera()
camera.rotation = 180

marca1 = time.time()

camera.resolution = (2592, 1944)
camera.capture('1.jpg')

marca2 = time.time()

camera.resolution = (1920, 1080)
camera.capture('2.jpg')

marca3 = time.time()

camera.resolution = (1280, 720)
camera.capture('3.jpg')

marca4 = time.time()

camera.resolution = (800, 600)
camera.capture('4.jpg')

marca5 = time.time()

camera.resolution = (640, 480)
camera.capture('5.jpg')

marca6 = time.time()

camera.resolution = (320, 240)
camera.capture('6.jpg')

marca7 = time.time()

print(marca2 - marca1, marca3 - marca2, marca4 - marca3, marca5 - marca4, marca6 - marca5, marca7 - marca6)