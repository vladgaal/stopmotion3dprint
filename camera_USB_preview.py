import cv2
import time

camera = cv2.VideoCapture(0)
camera.set(3, 1280)
camera.set(4, 720)

start = time.time()
return_value, image = camera.read()
cv2.imwrite('camera_USB_prev.png', image)
stop = time.time()

print("Time to take = ", stop - start, " s")

camera.release()