# Check the documentation for a complete description of the parameters:
# https://picamera.readthedocs.io/en/release-1.10/api_camera.html#picamera

from picamera import PiCamera
import time

camera = PiCamera()

camera.rotation = 180

camera.resolution = (1920, 1080)

camera.exposure_compensation = -3 # 6 * 1/6th = 1 stop

camera.exposure_mode = 'auto'

camera.awb_mode = 'auto'
camera.meter_mode = 'spot'
#camera.zoom  = (0.25, 0.25, 0.5, 0.5) # (x, y, w, h)

time.sleep(0.1)

start = time.time()
camera.capture('camera_module_prev.jpg')
stop = time.time()

print("Time to take = ", stop - start, " s")

camera.close()