import RPi.GPIO as GPIO
import sys, getopt
import time
from picamera import PiCamera
import Help

Led1Pin = 8    # pin8  --- Green led pin
Led2Pin = 10   # pin11 --- Yellow led pin
BtnPin = 12    # pin12 --- In signal pin

global numero
numero = 0     # shot number
global path
path = ''      # path to folder to save the Pic
global form
form = '.png'      # Pic extension


def setup():
    # Setup routine, run just at start
    
    global camera
    
    camera = PiCamera()    # Start the camera
    
    #if not camera._check_camera_open(): return False
    
    # run the "camera_module_preview.py" to check the parameters
    camera.rotation = 180
    camera.resolution = (1920, 1080)
    camera.exposure_compensation = -3 # 6 * 1/6th = 1 stop
    camera.exposure_mode = 'auto'
    camera.awb_mode = 'auto'
    camera.meter_mode = 'spot'
    #camera.zoom  = (0.25, 0.25, 0.5, 0.5) # (x, y, w, h)

    GPIO.setmode(GPIO.BOARD)        # Numbers GPIOs by physical location
    GPIO.setup(Led1Pin, GPIO.OUT)   # Set Green LedPin's mode is output
    GPIO.setup(Led2Pin, GPIO.OUT)   # Set Yellow LedPin's mode is output
    GPIO.setup(BtnPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)    # Set BtnPin's mode is input, and pull up to high level(3.3V)
    GPIO.output(Led1Pin, GPIO.LOW)  # Set Green LedPin low to off led
    GPIO.output(Led2Pin, GPIO.LOW)  # Set Yellow LedPin low to off led
  
    return True
    

def shot(ev=None):
    # Function executed whenever interruption occurs
    
    GPIO.output(Led2Pin, GPIO.HIGH) # Set Yellow LedPin high
    
    global numero
    global path
    global form
    
    camera.capture(path + 'Pic_' + str(numero).zfill(3) + form) # Save the image
    print('Shot: ' + str(numero).zfill(3))
    
    numero += 1
    time.sleep(0.1)
    
    GPIO.output(Led2Pin, GPIO.LOW) # Set Yellow LedPin low to off led
    

def loop():
    # Main loop routine
    
    GPIO.add_event_detect(BtnPin, GPIO.FALLING, callback=shot, bouncetime=200) # wait for falling and set bouncetime to prevent the callback function from being called multiple times when the button is pressed
    while True:
        #print('running')
        # Blink the green led
        time.sleep(1)
        GPIO.output(Led1Pin, GPIO.HIGH)
        time.sleep(1)
        GPIO.output(Led1Pin, GPIO.LOW)


def destroy():
    # Routine to close everything
    
    print('end')
    GPIO.output(Led1Pin, GPIO.LOW)     # led off
    GPIO.output(Led2Pin, GPIO.LOW)     # led off
    GPIO.cleanup()
    camera.close()


def main(argv):
    global path
    global form
    try:
        opts, args = getopt.getopt(argv,"hp:f:",["path=","form="])
    except getopt.GetoptError:
        print('Incorrect input')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            Help.show(index=1)
            sys.exit()
        elif opt in ("-p", "--path"):
            path = arg
        elif opt in ("-f", "--form"):
            form = arg
            
    print(path, form)
    
    if not setup():
        print("Problem loading camera")
        return False
    try:
        loop()
    except KeyboardInterrupt:  # When 'Ctrl+C' is pressed, the child program destroy() will be  executed.
        destroy()
    
    
if __name__ == '__main__':
    main(sys.argv[1:])
    
