import cv2

cam = cv2.VideoCapture(0)

cv2.namedWindow("test")

img_counter = 0

while True:
    ret, frame = cam.read()
    if not ret:
        print("failed to grab frame")
        break
    cv2.imshow("test", frame)

    k = cv2.waitKey(1)
    if k%256 == 27:
        # ESC pressed
        print("Escape hit, closing...")
        break
    elif k%256 == 32:
        # SPACE pressed
        img_name = "opencv_frame_{}.png".format(img_counter)
        cv2.imwrite(img_name, frame)
        print("{} written!".format(img_name))
        img_counter += 1

cam.release()

cv2.destroyAllWindows()

LedPin = 24    # pin11 --- led
BtnPin = 22    # pin12 --- button
Led_status = 1


def pisca():
    GPIO.output(LedPin, GPIO.LOW)
    time.sleep(1)
    for a in range(4):
        GPIO.output(LedPin, GPIO.HIGH)
        time.sleep(0.1)
        GPIO.output(LedPin, GPIO.LOW)
        time.sleep(0.5)


def setup():
    '''
    with open('/home/pi/botao_emergencia/lista.csv', 'r') as f:
        reader = csv.reader(f)
        global lista_destinatarios
        lista_destinatarios = list(reader)
    '''

    GPIO.setmode(GPIO.BOARD)       # Numbers GPIOs by physical location
    GPIO.setup(LedPin, GPIO.OUT)   # Set LedPin's mode is output
    GPIO.setup(BtnPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)    # Set BtnPin's mode is input, and pull up to high level(3.3V)
    pisca()
    GPIO.output(LedPin, GPIO.LOW) # Set LedPin high(+3.3V) to off led




def destroy():
    GPIO.output(LedPin, GPIO.LOW)  # led off
    GPIO.cleanup()  # Release resource
    cam.release()


def loop():
    cam = cv2.VideoCapture(0)
    img_counter = 0
    GPIO.add_event_detect(BtnPin, GPIO.FALLING, callback=take, bouncetime=200) # wait for falling and set bouncetime to prevent the callback function from being called multiple times when the button is pressed

    while True:
        GPIO.output(LedPin, GPIO.HIGH)
        ret, frame = cam.read()
        img_name = "opencv_frame_{}.png".format(img_counter)
        cv2.imwrite(img_name, frame)
        print("{} written!".format(img_name))
        img_counter += 1
        GPIO.output(LedPin, GPIO.LOW)


def take(ev=None):
    GPIO.output(LedPin, GPIO.HIGH)
    GPIO.output(LedPin, GPIO.HIGH)


if __name__ == '__main__':

    setup()

    try:
        loop()
    except KeyboardInterrupt:  # When 'Ctrl+C' is pressed, the child program destroy() will be  executed.
        destroy()
