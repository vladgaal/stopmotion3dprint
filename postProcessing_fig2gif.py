#!/usr/bin/env python

import os, sys, getopt
import imageio
import glob
import numpy as np
import cv2
import Help
import progressBar
import messenger


def rotate_image(image, angle):
    if angle != 0:
        image_center = tuple(np.array(image.shape[1::-1]) / 2)
        rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
        result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
        return result
    else:
        return image


def crop_image(image, crop_vec=(0, 0, 0, 0), do=False):
    if do:
        crop_img = image[crop_vec[1]:crop_vec[1] + crop_vec[3], crop_vec[0]:crop_vec[0] + crop_vec[2]]
        return crop_img
    else:
        return image


def convert_2_gif(path='', prefix='', name_out='movie', format='.jpg', crop=False, crop_vec=(0, 0, 0, 0), rotate=0):

    messenger.show(1,
                   rotate=rotate if rotate != 0 else False,
                   crop=crop_vec if crop else False,
                   prefix=prefix if prefix != '' else False,
                   path=path if path != '' else False,
                   output=name_out if name_out != '' else False)

    with imageio.get_writer(name_out + '.gif', mode='I') as writer:

        l = len(glob.glob(path + prefix + "*" + format))
        i = 0

        for filename in glob.glob(path + prefix + "*" + format):
            progressBar.printProgressBar(i + 1, l, prefix='Progress:', suffix='Complete', length=50)
            i += 1
            image0 = cv2.imread(filename)
            image1 = rotate_image(image0, rotate)
            image2 = crop_image(image1, crop_vec=crop_vec, do=crop)
            cv2.imwrite('temp.png', image2)
            image = imageio.imread('temp.png')
            writer.append_data(image)


def main(argv):

    try:
        opts, args = getopt.getopt(argv, "hi:n:o:f:cx:y:w:a:r:", ["inpath=","name=","outfile=","format=","vec_x=",
                                                                "vec_y=","vec_w=","vec_h=", "angle="])
    except getopt.GetoptError:
        print('Incorrect input')
        sys.exit(2)
    
    x, y, w, h = 0, 0, 0, 0
    crop = False
    format = '.jpg'
    outFile = 'output'
    path = ''
    name = 'Pic_'
    angle = 0

    for opt, arg in opts:
        if opt == '-h':
            Help.show(index=3)
            sys.exit()
        elif opt in ("-i", "--inpath"):
            path = arg
        elif opt in ("-n", "--name"):
            name = arg
        elif opt in ("-o", "--outfile"):
            outFile = arg
        elif opt in ("-f", "--format"):
            format = arg
        elif opt in ("-c", "--crop"):
            crop = True
        elif opt in ("-x", "--vecX"):
            x = int(arg)
        elif opt in ("-y", "--vecY"):
            y = int(arg)
        elif opt in ("-w", "--vecW"):
            w = int(arg)
        elif opt in ("-a", "--vecH"):
            h = int(arg)
        elif opt in ("-r", "--rotate"):
            angle = int(arg)

    if h == 0 and w == 0 and crop:
        print('Incorrect input, please specify all crop arguments')
        sys.exit(2)

    convert_2_gif(path=path, prefix=name, format=format, name_out=outFile, crop=crop, crop_vec=[x, y, w, h], rotate=angle)
    
    
if __name__ == '__main__':
    main(sys.argv[1:])